import React, { useState } from "react";
import "./Dropdown.scss";

const Dropdown = ({ list, title, toggleItem }) => {
	const [listOpen, setListOpen] = useState(false);
	const [headerTitle, setHeadertitle] = useState(title);

	const toggleList = () => {
		setListOpen(state => !state);
	};

	const handleItemClick = item => {
		toggleItem(item);
		setHeadertitle(item.name);
		setListOpen(false);
	};

	return (
		<div className="Dropdown">
			<div className="Dropdown_Header" onClick={() => toggleList()}>
				{headerTitle}
				<div className={`Dropdown_Header_Icon ${listOpen ? "up" : ""}`} />
			</div>
			{listOpen && (
				<ul className="Dropdown_List">
					{list.map(item => (
						<li
							key={item.name}
							className="Dropdown_List_Item"
							onClick={() => handleItemClick(item)}
						>
							{item.name}
						</li>
					))}
				</ul>
			)}
		</div>
	);
};

export default Dropdown;
