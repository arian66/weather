import React, { useState } from "react";
import Dropdown from "./core/Dropdown/Dropdown";
import MeteoGram from "./Meteogram/Meteogram";
import Table from "./Table/Table";
import { formatDate } from "./helpers/helpers";
import { locations } from "./data/locations";
import { columns } from "./data/tableLayout";
import "./App.scss";

const initialData = {
	labels: [],
	datasets: [
		{
			label: "rain probability",
			yAxisID: "rain",
			data: [],
			backgroundColor: "transparent",
			borderColor: "#156795"
		},
		{
			label: "temperature",
			yAxisID: "temp",
			data: [],
			backgroundColor: "transparent",
			borderColor: "#FB5136"
		}
	]
};

function App() {
	const [graphData, setGraphData] = useState(initialData);
	const [tableData, setTableData] = useState([]);

	const formatDataByKey = (array, key) => {
		let formatedArr = [];
		array.forEach(item => formatedArr.push(item[key]));
		return formatedArr;
	};

	const formatDataForGraph = (data, datasets) => {
		const timeZone = data[0].tz;
		const timesArr = formatDataByKey(data, "local_time").map(
			date => `${formatDate(date)} ${timeZone}`
		);
		const rainChanceArr = formatDataByKey(data, "rain_prob");
		const temperatureArr = formatDataByKey(data, "temperature");
		datasets[0].data = rainChanceArr;
		datasets[1].data = temperatureArr;
		return [timesArr, datasets];
	};

	const formatDataforTable = data => {
		const timeZone = data[0].tz;
		return data.map(item => ({
			time: `${formatDate(item.local_time)} ${timeZone}`,
			rain_prob: item.rain_prob,
			temperature: item.temperature,
			wind_speed: item.wind_speed,
			wind_direction: item.wind_direction
		}));
	};

	const handleFetchData = item => {
		fetch(
			`https://ws.weatherzone.com.au/?lt=aploc&lc=${item.code}&locdet=1&latlon=1&pdf=twc(period=48,detail=2)&u=1&format=json`
		)
			.then(res => res.json())
			.then(res => {
				const data = res.countries[0].locations[0].part_day_forecasts.forecasts;
				const [timesArr, datasets] = formatDataForGraph(
					data,
					initialData.datasets
				);
				setGraphData({
					...graphData,
					labels: timesArr,
					datasets
				});
				const tableData = formatDataforTable(data);
				setTableData(tableData);
			})
			.catch(err => console.log(err));
	};

	return (
		<div className="App">
			<div className="App_Sidebar">
				<Dropdown
					title="Select location"
					list={locations}
					toggleItem={handleFetchData}
					selected="Sydney"
				/>
			</div>
			<div className="App_Body">
				{tableData.length > 0 && (
					<>
						<MeteoGram width={600} height={300} data={graphData} />
						<Table columns={columns} data={tableData} />
					</>
				)}
			</div>
		</div>
	);
}

export default App;
