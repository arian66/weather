import React from "react";
import { Line } from "react-chartjs-2";

const options = {
	legend: {
		display: false
	},
	scales: {
		xAxes: [
			{
				ticks: {
					autoSkip: false
				}
			}
		],
		yAxes: [
			{
				id: "rain",
				type: "linear",
				position: "right",
				scaleLabel: {
					display: true,
					labelString: "Rain probability",
					fontColor: "#156795"
				}
			},
			{
				id: "temp",
				type: "linear",
				position: "left",
				scaleLabel: {
					display: true,
					labelString: "Temperature °C",
					fontColor: "#FB5136"
				}
			}
		]
	},
	maintainAspectRatio: false
};

const MeteoGram = ({ width, height, data }) => {
	return (
		<div>
			<Line data={data} width={width} height={height} options={options} />
		</div>
	);
};

export default MeteoGram;
