export const columns = [
	{
		accessor: "time",
		Header: "Time",
		class: "time"
	},
	{
		accessor: "rain_prob",
		Header: "Precipitation probability",
		class: "rain_prob"
	},
	{
		accessor: "temperature",
		Header: "Temperature °C"
	},
	{
		accessor: "wind_speed",
		Header: "Wind speed"
	},
	{
		accessor: "wind_direction",
		Header: "Wind Direction"
	}
];
