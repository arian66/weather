import React from "react";
import { useTable } from "react-table";
import "./Table.scss";

function Table({ columns, data }) {
	const {
		getTableProps,
		getTableBodyProps,
		headerGroups,
		rows,
		prepareRow
	} = useTable({
		columns,
		data
	});

	return (
		<table {...getTableProps()} className="Table">
			<thead className="Table_Header">
				{headerGroups.map(headerGroup => (
					<tr {...headerGroup.getHeaderGroupProps()}>
						{headerGroup.headers.map(column => (
							<th {...column.getHeaderProps()} className={column.class}>
								{column.render("Header")}
							</th>
						))}
					</tr>
				))}
			</thead>
			<tbody {...getTableBodyProps()} className="Table_Body">
				{rows.map((row, i) => {
					prepareRow(row);
					return (
						<tr {...row.getRowProps()}>
							{row.cells.map(cell => {
								return (
									<td
										{...cell.getCellProps()}
										className={cell.column.id}
										style={{
											transform:
												cell.column.id === "wind_direction"
													? `rotate(${cell.value}deg)`
													: "none"
										}}
									>
										{cell.column.id !== "wind_direction" && cell.render("Cell")}
									</td>
								);
							})}
						</tr>
					);
				})}
			</tbody>
		</table>
	);
}

export default Table;
