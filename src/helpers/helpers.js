export const getDayFromDate = date => {
	const days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
	const day = new Date(date).getDay();
	return days[day];
};

export const getTimeFromDate = date => {
	const d = new Date(date);
	return `${d.getHours()}:${d.getMinutes() < 10 ? "0" : ""}${d.getMinutes()}`;
};

export const formatDate = date => {
	return `${getDayFromDate(date)} ${getTimeFromDate(date)}`;
};
